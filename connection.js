var mysql = require('mysql');

function Connection(){
	this.pool = null;

	this.init = function(){
		this.pool = mysql.createPool({
			connectionLimit : 10,
			host: 'localhost',
			user: 'fcallata',
			password: 'fcallata',
			database: 'lavanderiaweb_db'
		});
	};

	this.acquire = function(callback){
		this.pool.getConnection(function(err, connection){			
			callback(err, connection);
		});
	};
}

module.exports = new Connection();