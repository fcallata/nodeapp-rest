var express = require('express');
var logger = require('morgan');
var bodyparser = require('body-parser');
var http = require('http');

var app = express();
app.use(function(req, res, next){
	res.header("Access-Control-Allow-Origin", "*");
	//res.header('Access-Control-Allow-Origin', req.get('origin'));
	//res.header("Access-Control-Allow-Credentials","true");
	res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE,OPTIONS');
	//res.header('Access-Control-Allow-Headers','Origin,Authorization, Content-Type, Content-lenght, X-Requested-With, Accept');
	res.header('Access-Control-Allow-Headers','Origin,X-Requested-With, Content-Type, Accept');
	//res.sendStatus(status);
	next();
})

app.use(logger('short'));
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());


//app.use(bodyparser.json({type:'application/json'}));


var connection = require('./connection');
var routesSM = require('./routes/supermarketRoutes');
var routes = require('./routes');

connection.init();
routes.configure(app);
routesSM.configure(app);

/*var server = app.listen(3000, function(){
	console.log("Guestbook app started on port 3000.");
});*/

http.createServer(app).listen(3000, function() {
	console.log("Guestbook app started on port 3000.");
});