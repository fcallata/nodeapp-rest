var connection = require('../connection');

function Users(){

	this.get = function(res){
		connection.acquire(function(err, con){
			con.query('select * from usuarios', function(err, result){
				con.release();
				res.send(result);
			});
		});
	};

	this.getone = function(id,res){
		connection.acquire(function(err, con){
			con.query('select * from usuarios where usuario_id = ?', [id], function(err, result){
				con.release();				
				res.send(result);
			});
		});
	};
	
	this.getName = function(name,res){
		connection.acquire(function(err, con){
			con.query('select * from usuarios where name like ?', '%'+[name]+'%', function(err, result){
				con.release();				
				res.send(result);
			});
		});
	};

	this.create = function(user, res){
		connection.acquire(function(err, con){
			var query ="insert into usuarios (name,lastname,email) values ('"+user.name+"','"+user.lastname+"','"+user.email+"')";
			con.query(query, function(err, result){
				con.release();
				if (err) {
					res.send({status:1, message:'User creation failed'});
				} else{
					res.send({status:0, message:'User created successfully'});
				};				
			});
		});
	};

	this.update = function(user, res){
		connection.acquire(function(err, con){
			con.query('update usuarios set ? where usuario_id = ?',[user, user.id], function(err, result){
				con.release();
				if (err) {
					res.send({status:1, message:'User update failed'});
				} else{
					res.send({status:0, message:'User update successfully'});
				};				
			});
		});
	};

	this.delete = function(id, res){
		connection.acquire(function(err, con){
			con.query('delete from usuarios where usuario_id = ?', [id], function(err, result){
				con.release();
				if (err) {
					res.send({status:1, message:'Failed delete user'});
				} else{
					res.send({status:0, message:'Delete user successfully'});
				};
			});
		});
	};
}

module.exports = new Users();