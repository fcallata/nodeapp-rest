var connection = require('../connection');

function Supermarket(){

	this.get = function(res){
		connection.acquire(function(err, con){
			con.query('select * from supermarkets', function(err, result){
				con.release();
				res.send(result);
			});
		});
	};
}

module.exports = new Supermarket();