var user = require('./models/users');

module.exports = {
	configure: function(app){

		app.get('/user/', function(req, res){			
			user.get(res);
		});

		app.get('/user/:id', function(req, res){
			console.log("valor de id: " + req.params.id);
			user.getone(parseInt(req.params.id), res);
		});

		app.get('/user/search/:name', function(req, res){
			console.log("valor de id: " + req.params.name);
			user.getName(req.params.name, res);
		});

		app.post('/user/', function(req, res){
			//var secret = req.body.secret;
			console.log("name: " + req.body.name);
			console.log("lastname: " + req.body.lastname);
			console.log("email:" + req.body.email);
			user.create(req.body, res);			
			//res.end('password' + req.body.name);
		});

		app.put('/user/', function(req, res){
			user.update(req.body, res);
		});

		app.delete('/user/:id', function(req, res){
			user.delete(req.params.id, res);
		});
	}
}